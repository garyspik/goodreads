package pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.concurrent.TimeUnit;

public class BasicPage {

    public static WebDriver driver = new ChromeDriver();

    public static void open(String url) {
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.navigate().to(url);
    }

    public static void wait(int seconds) throws InterruptedException {
        Thread.currentThread().sleep(seconds * 1000);
    }

    public static void is_text_visible(String text) {
        String bodyText = driver.findElement(By.tagName("body")).getText();
        Assert.assertTrue(bodyText.contains(text), "Text not found!");
    }

    public static void clear_cookies()
    {
        driver.manage().deleteAllCookies();
    }
    public static void log (String input){
        System.out.println(input);
    }

    public static void  click(String by){
        driver.findElement(By.xpath(by)).click();
    }

    public static  void  fillUp(String by, String text){
        driver.findElement(By.xpath(by)).sendKeys(text);
    }
}



