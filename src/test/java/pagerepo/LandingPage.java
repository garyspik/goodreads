package pagerepo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pagerepo.BasicPage;
import java.util.Random;

public class LandingPage extends  BasicPage{

    static String sign_up_name = "//input[@id='user_first_name']";
    static String sign_up_email = "//input[@id='user_email']";
    static String sign_up_pass = "//input[@id='user_password_signup']";
    static String sign_up_submit = "//input[@value='Sign up']";
    static String login_mail = "//input[@id='userSignInFormEmail']";
    static String login_pass = "//input[@id='user_password']";
    static String login_button = "//div[@class='formBox']/input[@type='submit']";


    public static void register(String login, String mail, String pass) throws InterruptedException {
        LandingPage.fillUp(sign_up_email,mail);
        LandingPage.fillUp(sign_up_name,login);
        LandingPage.fillUp(sign_up_pass,pass);
        LandingPage.click(sign_up_submit);
    }

    public static void login (String mail, String pass){
        LandingPage.fillUp(login_mail,mail);
        LandingPage.fillUp(login_pass,pass);
        LandingPage.click(login_button);
    }



}
