package pagerepo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;
import java.util.Random;

public class SearchResulPage extends BasicPage {

    private static String want_to_read_button = "//button[@class='wtrToRead']";
    static String button_actions_dropdown = "button.wtrShelfButton";
    static String button_action_read = "//button[@value='read']";
    static String review_field = "//textarea[@id='review_review_usertext']";
    static String review_star = "//div[@class='reviewFormEdit']//a[@class='star off']";
    private static String open_profile = "//div[@class='dropdown dropdown--profileMenu']";
    private static String sign_out = "//a[@href='/user/sign_out']";
    private static String start_year  = "//select[@class='rereadDatePicker smallPicker startYear']";
    private static String start_month ="//select[@class='rereadDatePicker largePicker startMonth']" ;
    private static String start_day = "//select[@class='rereadDatePicker smallPicker startDay']";
    private static String end_year = "//select[@class='rereadDatePicker smallPicker endYear']";
    private static String end_month = "//select[@class='rereadDatePicker largePicker endMonth']";
    private static String end_day = "//select[@class='rereadDatePicker smallPicker endDay']";
    private static String save_btn ="//div[@id='box']//input[@value='Save']";


    public static void mark_top_books_want_to_read(int amount) {
        List<WebElement> allbook = driver.findElements(By.xpath(want_to_read_button));
        for (int x = 0; x < amount; x = x + 1) {
            allbook.get(x).click();
        }
        System.out.println("Marked "+ amount + " book(s) as 'i want to read' ");
    }

    public static void mark_top_books_as_readed(int amount) throws InterruptedException {
        List<WebElement> alldropdowns = driver.findElements(new By.ByCssSelector(button_actions_dropdown));
        for (int x = 0; x < amount; x = x + 1) {
//            create random date for book start reading fields
            Random rand = new Random();
            int  rand_day = rand.nextInt(26) + 1;
            int  rand_month = rand.nextInt(10) + 2;
            int rand_year = 1930 + rand_day;
            alldropdowns.get(x).click();
            SearchResulPage.click(button_action_read);
            SearchResulPage.wait(1);
//            fill up Date started
            SearchResulPage.click(start_year);
            SearchResulPage.click("//option[@class='setDate' and contains(.," + rand_year+ ")]");
            SearchResulPage.click(start_month);
            SearchResulPage.click("//select[@class='rereadDatePicker largePicker startMonth']//option[@value="+ rand_month+"]");
//          increment date for Date finised fields
            rand_day= rand_day ++;
            rand_month = rand_month ++;
            rand_year = rand_year++;
//          fill up Date finished fields
            SearchResulPage.click(start_day);
            SearchResulPage.click("//select[@class='rereadDatePicker smallPicker startDay']//option[@value="+ rand_day+"]");
            SearchResulPage.click(end_year);
            SearchResulPage.click("//select[@class='rereadDatePicker smallPicker endYear']//option[@class='setDate' and contains(.," + rand_year+ ")]");
            SearchResulPage.click(end_month);
            SearchResulPage.click("//select[@class='rereadDatePicker largePicker endMonth']//option[@value="+ rand_month+"]");
            SearchResulPage.click(end_day);
            SearchResulPage.click("//select[@class='rereadDatePicker smallPicker endDay']//option[@value="+ rand_day+"]");
//            add review
            SearchResulPage.fillUp(review_field,"Autotest review");
//            add rating
            SearchResulPage.click(review_star);
            SearchResulPage.click(save_btn);
            SearchResulPage.wait(4);
            System.out.println("Mark one boook as read with review & rated ");
        }
    }

    public static void logout() throws InterruptedException {
        SearchResulPage.click(open_profile);
        SearchResulPage.click(sign_out);

    }


}

