import okhttp3.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.util.Random;


public class PetStoreTest {
    OkHttpClient client = new OkHttpClient();
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

    @Test
    public  void add_Pet() throws IOException {
        Random rand = new Random();
        int  rand_id = rand.nextInt(99999) + 1;
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"id\": "+ rand_id + ",\n  \"category\": {\n    \"id\": "+ rand_id +" ,\n    \"name\": \"string\"\n  },\n  \"name\": \"TestDog\",\n  \"photoUrls\": [\n    \"string\"\n  ],\n  \"tags\": [\n    {\n      \"id\": "+ rand_id +",\n      \"name\": \"string\"\n    }\n  ],\n  \"status\": \"available\"\n}");
        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "bd6355f5-5da6-d47d-46ab-e5ff3b6099d6")
                .build();
        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(),200);
        Assert.assertEquals(response.body().string(),"{\"id\":"+ rand_id +",\"category\":{\"id\":"+ rand_id +",\"name\":\"string\"},\"name\":\"TestDog\",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":"+rand_id+",\"name\":\"string\"}],\"status\":\"available\"}");
        System.out.println(response.code());
        System.out.println(rand_id);
    }

    @Test
    public void update_existed_pet() throws IOException {
        Random rand = new Random();
        int  rand_name_suf = rand.nextInt(99999) + 1;
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, "{\n  \"id\": 999,\n  \"category\": {\n    \"id\": 999,\n    \"name\": \"string" + rand_name_suf + "\"\n  },\n  \"name\": \"TestDog\",\n  \"photoUrls\": [\n    \"string" + rand_name_suf + "\"\n  ],\n  \"tags\": [\n    {\n      \"id\": 999,\n      \"name\": \"string" + rand_name_suf + "\"\n    }\n  ],\n  \"status\": \"available\"\n}");
        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet")
                .post(body)
                .addHeader("content-type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "cd030e64-734f-bf4b-50ac-b1a248a1d8c3")
                .build();
        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(),200);
        Assert.assertEquals(response.body().string(),"{\"id\":999,\"category\":{\"id\":999,\"name\":\"string" + rand_name_suf +"\"},\"name\":\"TestDog\",\"photoUrls\":[\"string" + rand_name_suf +"\"],\"tags\":[{\"id\":999,\"name\":\"string" + rand_name_suf +"\"}],\"status\":\"available\"}");
        System.out.println(response.code());
    }

    @Test
    public void get_pet_by_status() throws IOException {
        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet/findByStatus?status=autotesting")
                .get()
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "5055c1f2-c8c4-f1a1-be68-55a4caf4cbf5")
                .build();
        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(),200);
        Assert.assertEquals(response.body().string(),"[{\"id\":999,\"category\":{\"id\":999,\"name\":\"string\"},\"name\":\"TestDog\",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":999,\"name\":\"string\"}],\"status\":\"autotesting\"}]");
        System.out.println(response.code());
    }

    @Test
    public void get_pet_by_id () throws IOException{
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet/999")
                .get()
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "4981228a-9eb8-9dd1-b759-6ea2aaad5ff3")
                .build();

        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(), 200);
        Assert.assertEquals(response.body().string(),"[{\"id\":999,\"category\":{\"id\":999,\"name\":\"string\"},\"name\":\"TestDog\",\"photoUrls\":[\"string\"],\"tags\":[{\"id\":999,\"name\":\"string\"}],\"status\":\"autotesting\"}]");
        System.out.println(response.code());
    }

    @Test
    public void delete_pet_by_id (int pet_id) throws IOException{
        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet/"+ pet_id)
                .delete(null)
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "6ad916db-4c81-20e0-321d-21d528d6ab05")
                .build();

        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(), 200);
    }
    @Test
    public void upload_picture () throws IOException {
        File file = new File("pug.png");

        Request request = new Request.Builder()
                .url("https://petstore.swagger.io/v2/pet/999/uploadImage")
                .post(RequestBody.create(MEDIA_TYPE_PNG, file))
                .addHeader("cache-control", "no-cache")
                .addHeader("postman-token", "4981228a-9eb8-9dd1-b759-6ea2aaad5ff3")
                .build();
        Response response = client.newCall(request).execute();
        Assert.assertEquals(response.code(), 200);
        }
    }

