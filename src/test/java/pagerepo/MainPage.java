package pagerepo;

import org.openqa.selenium.By;

public class MainPage extends BasicPage {

    static String search_field = "//div[@class='searchBox searchBox--navbar']//input";
    static String search_button = "//div[@class='searchBox searchBox--navbar']//button";

    public static void search_for_book(String book_name) {
        MainPage.fillUp(search_field,book_name);
        MainPage.click(search_button);
    }

}
