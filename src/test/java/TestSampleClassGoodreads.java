import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pagerepo.LandingPage;
import pagerepo.MainPage;
import pagerepo.SearchResulPage;

public class TestSampleClassGoodreads {

// valid creds Oleg / garyspik21@gmail.com / Test!123

    @Test(priority = 10)
    public void login_test() throws InterruptedException {
        LandingPage.open("https://www.goodreads.com");
        LandingPage.register("Oleg", "onemoredemoreg@gmail.com", "Test!123");
        LandingPage.is_text_visible("Getting");
        System.out.println("Test completed");
    }
    @Test(priority = 20)
    public void user_story_1() throws InterruptedException {
        LandingPage.open("https://www.goodreads.com");
        LandingPage.login("garyspik21@gmail.com", "Test!121");
        LandingPage.is_text_visible("Sorry, we didn’t recognize that email/password combination. Please try again.");
        LandingPage.log("Incorect login - done");
        LandingPage.open("https://www.goodreads.com");
        LandingPage.login("garyspik21@gmail.com", "Test!123");
        MainPage.is_text_visible("CURRENTLY READING");
        MainPage.log("Login - done ");
        MainPage.search_for_book("Best crime and mystery books");
        SearchResulPage.is_text_visible("PAGE 1 OF ABOUT");
        SearchResulPage.log("Search - done");
        SearchResulPage.mark_top_books_want_to_read(3);
        SearchResulPage.mark_top_books_as_readed(3);
        SearchResulPage.logout();
        SearchResulPage.is_text_visible("You’ve been signed out");
        SearchResulPage.log("Signed out - done");

    }

    @AfterTest
     public void after_test(){
        System.out.println("Test completed");
        LandingPage.driver.close();
    }

}


